package unitedNationDocuments

import (
	"encoding/xml"
	"fmt"
	"strings"

	"bitbucket.org/matchmove/risk-document"
	"bitbucket.org/matchmove/risk-interests"
	"bitbucket.org/matchmove/utils"
)

//Source constants
const (
	SourceUNOrg = "www.un.org"
)

const (
	// FetchSubSetDatasModeNumeric mode to get the
	// subset that is separated by numeric bullets
	FetchSubSetDatasModeNumeric = "numeric"
	// FetchSubSetDatasModeAlphaNumeric mode to get
	// the subset that is separated by alpha numeric bullets
	FetchSubSetDatasModeAlphaNumeric = "alpha"
	// FetchSubSetDatasSeparatorColon colon separator
	FetchSubSetDatasSeparatorColon = ":"
	// FetchSubSetDatasSeparatorCloseParenthesis close parenthesis separator
	FetchSubSetDatasSeparatorCloseParenthesis = ")"
)

// UnitedNationDocument represent the file to be process
type UnitedNationDocument struct {
	documents.DocumentObject
	Reference   string
	Individuals []UnitedNationsIndividual
	Entities    []UnitedNationsEntity
}

func (e *UnitedNationsEntity) getAllCountry() []string {
	var countries []string
	for _, address := range e.Addresses {
		if address.Country != "" {
			countries = append(countries, utils.TrimNewlineSpaces(address.Country))
		}
	}
	return countries
}

func (i *UnitedNationsIndividual) getAllCountry() []string {
	var countries []string
	for _, address := range i.Addresses {
		if address.Country != "" {
			countries = append(countries, utils.TrimNewlineSpaces(address.Country))
		}
	}

	countries = append(countries, utils.TrimNewlineSpaces(i.PlaceOfBirth.Country))

	return countries
}

func (e *UnitedNationsEntity) format(reference string) interests.Interest {
	return interests.Interest{
		ID:               e.ID,
		Type:             interests.InterestTypeEntities,
		Reference:        reference,
		Source:           utils.TrimNewlineSpaces(SourceUNOrg + " - " + e.ListType.Value),
		Name:             utils.TrimNewlineSpaces(e.Name),
		OriginalScript:   utils.TrimNewlineSpaces(e.NameOriginalScript),
		AliasGoodQuality: e.Aliases.format("good"),
		AliasLowQuality:  e.Aliases.format("low"),
		Address:          e.Addresses.format(),
		ListedOn:         utils.TrimNewlineSpaces(e.ListedOn),
		OtherInformation: utils.TrimNewlineSpaces(e.OtherInformation),
		Country:          e.getAllCountry(),
	}
}

func (i *UnitedNationsIndividual) format(reference string) interests.Interest {
	return interests.Interest{
		ID:               utils.TrimNewlineSpaces(i.ID),
		Type:             interests.InterestTypeIndividual,
		Source:           utils.TrimNewlineSpaces(SourceUNOrg + " - " + i.ListType.Value),
		Reference:        reference,
		Name:             utils.TrimNewlineSpaces(i.FirstName + " " + i.SecodeName + " " + i.ThirdName + " " + i.FourthName),
		OriginalScript:   utils.TrimNewlineSpaces(i.NameOriginalScript),
		Nationality:      []string{utils.TrimNewlineSpaces(i.Nationality.Value)},
		AliasGoodQuality: i.Aliases.format("good"),
		AliasLowQuality:  i.Aliases.format("low"),
		Address:          i.Addresses.format(),
		ListedOn:         utils.TrimNewlineSpaces(i.ListedOn),
		LastUpdated:      utils.TrimNewlineSpaces(i.LastDayUpdated.Value),
		OtherInformation: utils.TrimNewlineSpaces(i.OtherInformation),
		Country:          i.getAllCountry(),
		Title:            utils.TrimNewlineSpaces(i.Title.Value),
		Designation:      utils.TrimNewlineSpaces(i.Designation.Value),
		DOB:              []string{i.Birthday.Year},
		POBTown:          utils.TrimNewlineSpaces(i.PlaceOfBirth.Street + " " + i.PlaceOfBirth.City + " " + i.PlaceOfBirth.StateProvince + " " + i.PlaceOfBirth.ZipCode),
		POBCountry:       utils.TrimNewlineSpaces(i.PlaceOfBirth.Country),
		Documents:        utils.ArrayUnique([]string{utils.TrimNewlineSpaces(i.Documents.TypeOfDocument1), utils.TrimNewlineSpaces(i.Documents.TypeOfDocument2)}),
	}
}

// NewUnitedNationDocument initilized UnitedNationDocument
func NewUnitedNationDocument(f documents.File) UnitedNationDocument {
	f.ReadFile()

	document := UnitedNationDocument{}
	document.Body = f.Contents
	document.Reference = f.Path + f.Filename
	return document
}

// Parse breaks d.Body into arrays
func (d *UnitedNationDocument) Parse() {
	var query UnitedNationsQuery
	decoder := xml.NewDecoder(strings.NewReader(d.Body))
	decoder.Strict = false
	decoder.Decode(&query)

	d.Individuals = query.UnitedNationsIndividuals.Individuals
	d.Entities = query.UnitedNationsEntities.Entities
}

// Format parse the array in d.Entities and d.Individuals
// and assigns them to an array of Interest
func (d *UnitedNationDocument) Format() interests.Interests {
	var interests interests.Interests

	for _, individual := range d.Individuals {
		interests = append(interests, individual.format(d.Reference))
	}

	for _, entity := range d.Entities {
		interests = append(interests, entity.format(d.Reference))
	}

	return interests
}

// UnitedNationsQuery struct for United Nations
// United Nations xml struct the entire document
type UnitedNationsQuery struct {
	XMLName                  xml.Name                 `xml:"CONSOLIDATED_LIST"`
	UnitedNationsIndividuals UnitedNationsIndividuals `xml:"INDIVIDUALS"`
	UnitedNationsEntities    UnitedNationsEntities    `xml:"ENTITIES"`
}

// UnitedNationsIndividuals xml struct the UnitedNationsIndividuals
type UnitedNationsIndividuals struct {
	XMLName     xml.Name                  `xml:"INDIVIDUALS"`
	Individuals []UnitedNationsIndividual `xml:"INDIVIDUAL"`
}

// UnitedNationsEntities xml struct the UnitedNationsEntities
type UnitedNationsEntities struct {
	XMLName  xml.Name              `xml:"ENTITIES"`
	Entities []UnitedNationsEntity `xml:"ENTITY"`
}

// UnitedNationsIndividual United Nations xml struct for individuals
type UnitedNationsIndividual struct {
	XMLName            xml.Name                            `xml:"INDIVIDUAL"`
	ID                 string                              `xml:"REFERENCE_NUMBER"`
	DataID             string                              `xml:"DATAID"`
	VersionNumber      string                              `xml:"VERSIONNUM"`
	FirstName          string                              `xml:"FIRST_NAME"`
	SecodeName         string                              `xml:"SECOND_NAME"`
	ThirdName          string                              `xml:"THIRD_NAME"`
	FourthName         string                              `xml:"FOURTH_NAME"`
	NameOriginalScript string                              `xml:"NAME_ORIGINAL_SCRIPT"`
	UnListType         string                              `xml:"UN_LIST_TYPE"`
	ListedOn           string                              `xml:"LISTED_ON"`
	OtherInformation   string                              `xml:"COMMENTS1"`
	SortKey            string                              `xml:"SORT_KEY"`
	SortKeyLastMod     string                              `xml:"SORT_KEY_LAST_MOD"`
	Birthday           UnitedNationsIndividualBirthday     `xml:"INDIVIDUAL_DATE_OF_BIRTH"`
	PlaceOfBirth       UnitedNationsIndividualPlaceOfBirth `xml:"INDIVIDUAL_PLACE_OF_BIRTH"`
	Documents          UnitedNationsIndividualDocument     `xml:"INDIVIDUAL_DOCUMENT"`
	Title              UnitedNationsTitle                  `xml:"TITLE"`
	Designation        UnitedNationsDesignation            `xml:"DESIGNATION"`
	Nationality        UnitedNationsNationality            `xml:"NATIONALITY"`
	ListType           UnitedNationsListType               `xml:"LIST_TYPE"`
	LastDayUpdated     UnitedNationsLastDayUpdated         `xml:"LAST_DAY_UPDATED"`
	Aliases            UnitedNationsIndividualAliases      `xml:"INDIVIDUAL_ALIAS"`
	Addresses          UnitedNationsIndividualAddresses    `xml:"INDIVIDUAL_ADDRESS"`
}

// UnitedNationsEntity United Nations xml struct for entities
type UnitedNationsEntity struct {
	XMLName            xml.Name                     `xml:"ENTITY"`
	ID                 string                       `xml:"REFERENCE_NUMBER"`
	DataID             string                       `xml:"DATAID"`
	VersionNumber      string                       `xml:"VERSIONNUM"`
	Name               string                       `xml:"FIRST_NAME"`
	NameOriginalScript string                       `xml:"NAME_ORIGINAL_SCRIPT"`
	UnListType         string                       `xml:"UN_LIST_TYPE"`
	ListedOn           string                       `xml:"LISTED_ON"`
	OtherInformation   string                       `xml:"COMMENTS1"`
	SortKey            string                       `xml:"SORT_KEY"`
	SortKeyLastMod     string                       `xml:"SORT_KEY_LAST_MOD"`
	ListType           UnitedNationsListType        `xml:"LIST_TYPE"`
	Aliases            UnitedNationsEntityAliases   `xml:"ENTITY_ALIAS"`
	Addresses          UnitedNationsEntityAddresses `xml:"ENTITY_ADDRESS"`
}

// UnitedNationsTitle United Nations xml struct for title
type UnitedNationsTitle struct {
	XMLName xml.Name `xml:"TITLE"`
	Value   string   `xml:"VALUE"`
}

// UnitedNationsDesignation United Nations xml struct for Designation
type UnitedNationsDesignation struct {
	XMLName xml.Name `xml:"DESIGNATION"`
	Value   string   `xml:"VALUE"`
}

// UnitedNationsNationality United Nations xml struct for Nationality
type UnitedNationsNationality struct {
	XMLName xml.Name `xml:"NATIONALITY"`
	Value   string   `xml:"VALUE"`
}

// UnitedNationsListType United Nations xml struct for list type
type UnitedNationsListType struct {
	XMLName xml.Name `xml:"LIST_TYPE"`
	Value   string   `xml:"VALUE"`
}

// UnitedNationsLastDayUpdated United Nations xml struct for list type
type UnitedNationsLastDayUpdated struct {
	XMLName xml.Name `xml:"LAST_DAY_UPDATED"`
	Value   string   `xml:"VALUE"`
}

// UnitedNationsIndividualPlaceOfBirth United Nations xml struct for place of birth
type UnitedNationsIndividualPlaceOfBirth struct {
	XMLName       xml.Name `xml:"INDIVIDUAL_PLACE_OF_BIRTH"`
	Street        string   `xml:"STREET"`
	City          string   `xml:"CITY"`
	StateProvince string   `xml:"STATE_PROVINCE"`
	ZipCode       string   `xml:"ZIP_CODE"`
	Country       string   `xml:"COUNTRY"`
}

// UnitedNationsIndividualDocument United Nations xml struct for document
type UnitedNationsIndividualDocument struct {
	XMLName         xml.Name `xml:"INDIVIDUAL_DOCUMENT"`
	TypeOfDocument1 string   `xml:"TYPE_OF_DOCUMENT"`
	TypeOfDocument2 string   `xml:"TYPE_OF_DOCUMENT2"`
}

// UnitedNationsIndividualBirthday United Nations xml struct for birthday
type UnitedNationsIndividualBirthday struct {
	XMLName xml.Name `xml:"INDIVIDUAL_DATE_OF_BIRTH"`
	Year    string   `xml:"YEAR"`
}

// UnitedNationsIndividualAlias United Nations xml struct for alias
type UnitedNationsIndividualAlias struct {
	XMLName   xml.Name `xml:"INDIVIDUAL_ALIAS"`
	Quality   string   `xml:"QUALITY"`
	AliasName string   `xml:"ALIAS_NAME"`
}

// UnitedNationsIndividualAliases array of UnitedNationsIndividualAlias
type UnitedNationsIndividualAliases []UnitedNationsIndividualAlias

func (aliases UnitedNationsIndividualAliases) format(aType string) []string {
	var alias []string
	for _, item := range aliases {
		if item.Quality == aType {
			alias = append(alias, utils.TrimNewlineSpaces(item.AliasName))
		}
	}
	return alias
}

// UnitedNationsIndividualAddress United Nations xml struct for address
type UnitedNationsIndividualAddress struct {
	XMLName       xml.Name `xml:"INDIVIDUAL_ADDRESS"`
	Note          string   `xml:"NOTE"`
	Street        string   `xml:"STREET"`
	City          string   `xml:"CITY"`
	StateProvince string   `xml:"STATE_PROVINCE"`
	ZipCode       string   `xml:"ZIP_CODE"`
	Country       string   `xml:"COUNTRY"`
}

// UnitedNationsIndividualAddresses array of UnitedNationsIndividualAddress
type UnitedNationsIndividualAddresses []UnitedNationsIndividualAddress

func (a UnitedNationsIndividualAddresses) format() []string {
	var addresses []string

	for _, item := range a {
		addresses = append(
			addresses,
			utils.TrimNewlineSpaces(fmt.Sprintf("%s %s %s %s %s", item.Street, item.City, item.StateProvince, item.ZipCode, item.Country)),
		)
	}

	return utils.ArrayUnique(addresses)
}

// UnitedNationsEntityAlias United Nations xml struct for alias
type UnitedNationsEntityAlias struct {
	XMLName   xml.Name `xml:"ENTITY_ALIAS"`
	Quality   string   `xml:"QUALITY"`
	AliasName string   `xml:"ALIAS_NAME"`
}

// UnitedNationsEntityAliases array of UnitedNationsEntityAlias
type UnitedNationsEntityAliases []UnitedNationsEntityAlias

func (a UnitedNationsEntityAliases) format(aType string) []string {
	var aliases []string
	for _, alias := range a {
		if alias.Quality == aType {
			aliases = append(aliases, utils.TrimNewlineSpaces(alias.AliasName))
		}
	}

	return utils.ArrayUnique(aliases)
}

// UnitedNationsEntityAddress United Nations xml struct for address
type UnitedNationsEntityAddress struct {
	XMLName       xml.Name `xml:"ENTITY_ADDRESS"`
	Note          string   `xml:"NOTE"`
	Street        string   `xml:"STREET"`
	City          string   `xml:"CITY"`
	StateProvince string   `xml:"STATE_PROVINCE"`
	ZipCode       string   `xml:"ZIP_CODE"`
	Country       string   `xml:"COUNTRY"`
}

// UnitedNationsEntityAddresses array of UnitedNationsEntityAddress
type UnitedNationsEntityAddresses []UnitedNationsEntityAddress

func (a UnitedNationsEntityAddresses) format() []string {
	var addresses []string

	for _, item := range a {
		addresses = append(
			addresses,
			utils.TrimNewlineSpaces(fmt.Sprintf("%s %s %s %s %s", item.Street, item.City, item.StateProvince, item.ZipCode, item.Country)),
		)
	}

	return utils.ArrayUnique(addresses)
}
