package unitedNationDocuments

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	"bitbucket.org/matchmove/risk-document"
)

const (
	ProcessorSource = "documents/source/"
)

func TestUnitedNationDocument(t *testing.T) {

	files, err := documents.GetLatestFiles(ProcessorSource)
	if err != nil {
		t.Errorf("Exception occured in files.GetLatest: %s", err)
	}

	for _, file := range files {
		if strings.Contains(strings.ToLower(file.Filename), SourceUNOrg) {
			document := NewUnitedNationDocument(file)

			if "main.UnitedNationDocument" != fmt.Sprint(reflect.TypeOf(document)) {
				t.Errorf("UnitedNationDocument instance expecting:%s, got:%s", "main.UnitedNationDocument", string(fmt.Sprint(reflect.TypeOf(document))))
			}

			document.Parse()
			interests := document.Format()
			if len(interests) == 0 {
				t.Errorf("document.Format is expected to have more than 1 item")
			}
		}

	}

}
